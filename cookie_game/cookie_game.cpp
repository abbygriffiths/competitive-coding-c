#include <algorithm>
#include <iostream>
#include <set>
#include <vector>

using namespace std;

int test_case_score();
void result(vector<int> test_case);

int main()
{
  int test_cases = 0;
  cin >> test_cases;

  vector< vector<int> > tests(test_cases);

  for_each(begin(tests), end(tests),
           [](vector<int>& test_case) {
             int players = 0;
             cin >> players;
             test_case.resize(players);

             for_each(begin(test_case), end(test_case),
                      [](int& player_score) {
                        player_score = test_case_score();
                      });
             result(test_case);
           });

  return 0;
}

int test_case_score()
{
  int cookie_count = 0, pts = 0;
  cin >> cookie_count;
                        
  vector<int> cookies(cookie_count);
  for_each(begin(cookies), end(cookies), [](int& n) { cin >> n; });
                        
  set<int> unique_cookies(begin(cookies), end(cookies));
  pts = cookies.size();
          
  switch (unique_cookies.size())
    {
    case 4:
      pts += 1;
      break;

    case 5:
      pts += 2;
      break;

    case 6:
      pts += 4;
      break;
    }

  return pts;
}


void result(vector<int> test_case)
{
  int high_score = *(std::max_element(begin(test_case), end(test_case)));
  int idx = std::distance(begin(test_case), std::find(begin(test_case), end(test_case), high_score));

  if (std::count(begin(test_case), end(test_case), high_score) > 1)
    {
      cout << "tie";
    }
  else if (idx)
    {
      cout << idx + 1;
    }
  else
    {
      cout << "chef";
    }
  cout << endl;
  return;
}

