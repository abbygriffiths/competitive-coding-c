/* Bookshelves: A program to take input and give output for lowest Skew count.
   @Author: Abirbhav Goswami
   @Dependencies: None */

#include <algorithm>
#include <iostream>
#include <vector>

using std::begin;
using std::cin;
using std::cout;
using std::end;
using std::endl;
using std::vector;

template <typename T>
auto max(vector<T>& v)
{
  return std::max_element(begin(v), end(v));
}

template <typename T>
auto min(vector<T>& v)
{
  return std::min_element(begin(v), end(v));
}

template <typename T>
void make_swap(vector<T>& v1, vector<T>& v2)
{
}

int main()
{
  int n = 0, k = 0;
  cin >> n >> k;
  vector<int> shelf1(n), shelf2(n);

  std::for_each(begin(shelf1), end(shelf1), [](int& elem) {
      cin >> elem;
    });
  
  std::for_each(begin(shelf2), end(shelf2), [](int& elem) {
      cin >> elem;
    });

  std::sort(begin(shelf1), end(shelf1));
  std::sort(begin(shelf2), end(shelf2));
  
  for (int i = 0; i < k; i++)
    {
      auto max1 = max(shelf1);
      auto max2 = max(shelf2);

      auto min1 = min(shelf1);
      auto min2 = min(shelf2);
      
      if (*max1 > *max2 && (*min1 < *max2))
        {
          int tmp = *max2;
          *max2 = *min1;
          *min1 = tmp;
        }
      else if (*max2 > *max1 && (*min2 < *max1))
        {
          int tmp = *max1;
          *max1 = *min2;
          *min2 = tmp;
        }
      else
        {
          break;
        }
    }
 
  auto skew = *max(shelf1) + *max(shelf2);
  cout << skew << endl;

  return 0;
}

